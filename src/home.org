#+title: Mohammed Sadiq's Website
#+date: <2018-08-23 Thu 08:27>
#+created: <2018-08-08 Wed 00:01>
#+creator: home
#+setupfile: include/home.org
#+description: Website of Mohammed Sadiq
#+export_file_name: index
#+html_head: <link href="css/site.css" rel="stylesheet">
#+html_head_extra: <link href="https://www.sadiqpk.org" rel="canonical">
#+options: H:3

* About

  {{{info(danger, <i>Warning:</i> The website design is a work in progress.
  You may find wrong information\, broken links and designs.)}}}

  Hello, I’m Mohammed Sadiq, a [[https://www.gnu.org/philosophy/free-sw.html][free software]] developer from [[https://en.wikipedia.org/wiki/Kerala][Kerala]],
  mostly interested in C programming, [[https://www.gnome.org][GNOME]], and [[https://www.gnu.org/s/emacs][GNU Emacs]].  This is my personal
  online space where I write and advertise my works.

  [[https://www.sadiqpk.org][This website]] has been written in Emacs org-mode and nothing else.  The
  design and all the [[./blog/index.html][blog posts]] here are made available as public domain 
  ([[https://creativecommons.org/publicdomain/zero/1.0/][CC0]]) unless otherwise specified.

  Some of the free software projects I wrote are published in the
  [[./projects/index.html][Projects page]].  Some of them are available for use under
  the terms of GNU GPLv3+ and some as CC0 public domain dedication.

  # I do occasionally write [[./diary/index.html][diary]] in [[https://en.wikipedia.org/wiki/Malayalam][Malayalam]].
  # You are free to use them, even commercially but under the terms of
  # [[https://creativecommons.org/licenses/by-nd/4.0/][CC BY-ND 4.0]].
