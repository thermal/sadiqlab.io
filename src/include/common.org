#+author: Mohammed Sadiq
#+language: en
#+options: toc:nil num:nil tasks:done todo:nil title:nil
#+html_doctype: html5
#+options: html-style:nil html-scripts:nil
#+macro:key (eval (my-web-get-keys '("$1" "$2" "$3" "$4")))
#+macro:info @@html:<span class="info $1">$2 $3 $4</span>@@
#+macro:gitlab [[https://gitlab.com/sadiq/$1][$2]]
#+macro:hn [[https://news.ycombinator.com/item?id=$1][$2]]
#+macro:git [[https://gitlab.com/sadiq/$1][GitLab]] [[https://github.com/pksadiq/$1][GitHub]]
