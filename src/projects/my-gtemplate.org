#+title: My GTemplate
#+date: <2018-08-27 Mon 16:46>
#+created: <2018-08-27 Mon 16:46>
#+creator: projects/my-gtemplate
#+setupfile: ../include/projects.org
#+keywords: gtk+, gtk3, gtk4, c, mesonbuild, template, gnu, linux, windows
#+description: A Simple feature rich GTK3/GTK4 template


* Introduction

  My GTemplate is simple but feature rich GUI Application template
  written in C using GTK3/GTK4.  Several features are included
  along with a script to bootstrap your own project.

  The project is released into public domain (CC0).  Feel free to use
  it the way you wish.  If you wish to support me, consider
  [[https://liberapay.com/sadiq/donate][donating me via librepay]].

  If you wish to learn GTK+, see the [[https://developer.gnome.org/gnome-devel-demos/stable/c.html.en][GNOME Platform Demos]] and
  Getting started with [[https://developer.gnome.org/gtk3/3.22/gtk-getting-started.html][GTK3]].

  #+caption: Obligatory screenshot
  #+attr_html: :width 100% :align center
  [[./img/my-gtemplate.png]]

  #+caption: The same on Windows 8
  #+attr_html: :width 100% :align center
  [[./img/my-gtemplate-w32.png]]

  Source: {{{git(my-gtemplate)}}}


* Features

  - Written in C programming language
  - [G]Object Oriented Design
  - Supports GTK3 and GTK4
  - [[https://mesonbuild.com][Meson]] build system with fallback configure script
  - [[https://flatpak.org][Flatpak]] support included
  - GitLab CI configuration included
  - [[#w32-support][Microsoft Windows support]] with [[http://www.mingw.org][MinGW]] and [[https://wiki.gnome.org/msitools][msitools]]
  - GtkBuilder .ui templates for UI
  - UI based on [[https://developer.gnome.org/hig/stable][GNOME HIG]]
    - GNOME shell menu not included as [[https://wiki.gnome.org/Design/Whiteboards/AppMenuMigration][it’s deprecated]]
  - Application specific CSS
  - Keyboard shortcuts window
  - Handles commandline arguments
  - Tab completion for commandline
  - Internationalisation using gettext
  - Documentations for code using gtk-doc
  - [[http://www.projectmallard.org/][Mallard]] based help for GNOME Help
  - Manpage in docbook XML format
  - GSettings integration
  - Symbolic and non-symbolic icons
  - Script to generate non-symbolic icons from symbolic ones
  - [[https://github.com/uncrustify/uncrustify][Uncrustify]] configuration and script
  - Meson based unit tests
  - And many more…

  Follow the instructions below to compile the project from
  source:

* Install pre-requisites

  To build the project you require a recent version of
  some GNU/Linux.
  Feel free to use the OS of your choice.  Though it’s
  recommended to use the latest version Ubuntu, Fedora
  or Debian testing.

  To bootstrap a new project, a few packages are required:

  #+begin_src sh
  # On Debian, Ubuntu and derivatives
  sudo apt install -y git meson flatpak flatpak-builder \
  uncrustify build-essential libgtk-3-dev appstream-util \
  xsltproc docbook-xsl gedit gnome-builder

  # On Fedora and derivatives
  sudo dnf install -y @c-development @development-tools \
  gettext-devel gtk3-devel uncrustify gettext-devel meson \
  flatpak flatpak-builder desktop-file-utils git gedit \
  docbook-style-xsl gnome-builder
  #+end_src

  Many developers use GNU Emacs or vim as Editor.  If you
  don’t have such an editor of choice, it’s recommended to
  use [[https://wiki.gnome.org/Apps/Builder][GNOME Builder]].  You may also find Flatpak helpful when
  developing with GNOME Builder.  To configure Flatpak, see
  [[https://flatpak.org/setup/][Flatpak setup]] guide.

  If you are planning to build for Microsoft Windows, Fedora
  may be the only OS choice as it includes many MinGW packages
  required for building GTK3 applications.


* Creating a new project from template

  To create a new project from the template, clone the
  [[https://gitlab.com/sadiq/my-gtemplate][repository]], edit ~new-project.sh~
  script and replace the default values with your project
  specific values.

  #+begin_src sh
  # Clone the repository
  ~$ git clone https://gitlab.com/sadiq/my-gtemplate.git
  ~$ cd my-gtemplate

  # Edit the file 'new-project.sh'
  ~/my-gtemplate$ gedit new-project.sh
  #+end_src

  Let’s say we are creating a GTK3 project for managing notes.
  Let’s choose some (in)sane names for our project and change
  ~new-project.sh~ accordingly:

  #+begin_src sh
  # Make necessary changes to the variables.
  # Only some of them are listed here.
  GTK_VERSION="gtk3"
  APP_NAME="gee-note"
  APP_LAST_NAME="note"
  APP_REAL_NAME="Gee Note"
  # Assuming your repo is https://gitlab.com/username/gee-note
  APP_ID="com.gitlab.username.gee-note"
  APP_SHRT="gnt"
  APP_SHRT_CAP="GNt"
  #+end_src

  Now let’s try to build a template for our project:

  #+begin_src sh
  # Make necessary changes to new-project.sh and run
  ~/my-gtemplate$ ./new-project.sh

  # APP_NAME will be used as directory name, so for us
  # it will be gee-note.  Move it
  ~/my-gtemplate$ mv gee-note ~/
  ~/my-gtemplate$ cd ~/gee-note

  # Now compile using meson
  ~/gee-note$ meson _build && cd _build
  ~/gee-note/_build$ ninja && sudo ninja install

  # Or alternatively, use the configure script.
  # All it does is to run meson.
  ~/gee-note$ ./configure && make && sudo make install
  # Let’s now run it
  ~/gee-note$ gee-note
  #+end_src

** Building for Microsoft Windows
   :PROPERTIES:
   :CUSTOM_ID: w32-support
   :END:

   A build script to compile and build ~.msi~ installer is
   included in the repository.  The build script make use of
   [[http://www.mingw.org][MinGW]] packages to compile the source, and [[https://wiki.gnome.org/msitools][msitools]] to create
   an ~.msi~ installer out of it.  You don’t require to have
   access to any Windows machine to compile or build the installer.
   All the process can be done on a Fedora GNU/Linux.

   {{{info(danger,The script has been tested only with Fedora 28.
   And only GTK3 is supported.)}}}

   First, install the required packages:
   #+begin_src sh
   # Instal mingw64-gtk3 for 64 bit build
   sudo dnf install -y mingw32-gtk3 meson gcc gtk3-devel msitools
   #+end_src

   To build, Update the file ~build-aux/w32/defaults.sh.in~ and adapt
   the variables as per your needs and do:
   #+begin_src sh
   # Compile
   ~/gee-note$ ./build-aux/w32/make-w32.sh
   # Alternately, give arguments to specify details
   # First argument: Whether the build has to be optimized
   # Posible values (arg1): yes no
   # Second argument: Architecture the program should run
   # Possible values (arg2): i686 x86_64
   ~/gee-note$ ./build-aux/w32/make-w32.sh yes i686

   # And then follow the instructions printed by the
   # script to create an msi installer.
   #+end_src

   {{{info(danger,The generated .msi installer has a size
   of about 26 MiB.  Around 18 MiB of that is Adwaita icon
   theme.  You may be able to optimize the size if you include
   a custom version of Adwaita with only the required icons.
   You can also strip the binaries which can further save
   around 1-2 MiB.)}}}

   You may use [[https://www.winehq.org][wine]] to test the installer and the executable:

   #+begin_src sh
   # Install the .msi file.  Use the
   # right .msi path shown in previous step
   ~/$ wine msiexec /i /path/to/installer.msi /l*v log.txt

   # Run the executable.  Adjust the path if needed
   ~$ cd "$HOME/.wine/drive_c/Program Files/My GTemplate"
   .../$ XDG_DATA_DIRS="./share" wine bin/my-gtemplate.exe
   #+end_src

   And on Windows, as usual, you can simply double click
   the ~.msi~ file to install.

* Required changes for new projects

  The project created by running ~new-project.sh~ shall still
  have stale data.  So, those files should be updated to match
  your project.  At the minimum, the following list of files are
  thus supposed to be changed (The items in paren suggests the
  alternate name you might have set in ~new-project.sh~.):

  - All ~README.md~ files
  - ~my-gtemplate.doap~ (~APP_NAME~)
  - ~docs/help/C/~ (All files)
  - ~docs/man/my-gtemplate.xml~ (~APP_NAME~)
    - dummy examples and descriptions
  - ~desktop.in~ file in ~data~ (~APP_ID~)
    - At least the ~Comment~ and ~Keywords~ values
  - Image in ~data/icons/hicolors/calable/apps/~
    - And then run ~data/icons/hicolor/render-icons.sh~ script
  - ~data/appdata/~ (All files, except ~meson.build~)
  - ~src/resources/ui/mgt-window.ui~ (~APP_SHRT~)
    - ~subtitle~ in ~GtkHeaderBar~

  Of course, that’s just the minimum change.  When you develop,
  you’ll have to change everything in ~src/~ directory, and
  probably other directories too.


* License

  Written in 2017, 2018 by Mohammed Sadiq <sadiq[at]sadiqpk[d0t]org>

  #+include: ../include/cc0.org
