/*
 * Hello world with GTK+ in C
 *
 * By Mohammed Sadiq <sadiq [at] sadiqpk [d0t] org>
 * Date: 2018 October 25
 * License: None, Public domain (CC0)
 *
 * Compile (Replace gtk+-3.0 with gtk+-4.0 for GTK4):
 * gcc $(pkg-config --cflags --libs gtk+-3.0) hello.c -o hello
 *
 * Run:
 * ./hello
 */

#include <gtk/gtk.h>

static void
activate_cb (GtkApplication *application,
             gpointer        user_data)
{
  GtkWidget *window;
  GtkWidget *label;

  window = gtk_application_window_new (application);
  gtk_window_set_title (GTK_WINDOW (window), "Hello World!");
  gtk_window_set_default_size (GTK_WINDOW (window), 400, 300);

  label = gtk_label_new ("Hello World!");
  gtk_container_add (GTK_CONTAINER (window), label);
  /* Not required for GTK+-4 */
  gtk_widget_show (label);

  gtk_window_present (GTK_WINDOW (window));
}

int
main (int    argc,
      char **argv)
{
  g_autoptr(GtkApplication) application = NULL;

  application = gtk_application_new ("org.sadiqpk.example",
                                     G_APPLICATION_FLAGS_NONE);
  g_signal_connect (application, "activate",
                    G_CALLBACK (activate_cb), NULL);

  return g_application_run (G_APPLICATION (application),
                            argc, argv);
}
